import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardGuard } from './guards/auth-guard.guard';

const routes: Routes = [
  {
    path:'',
    redirectTo:'/home',
    pathMatch: 'full' 
  },
  {
    path:'home',
    loadChildren : ()=>import('./home/home.module').then(m => m.HomeModule),
    canActivate:[AuthGuardGuard]
  },
  {
    path:'home/agregarMascota',
    loadChildren : ()=>import('./add-pets/add-pets.module').then(m => m.AddPetsModule),
    canActivate:[AuthGuardGuard]
  },
  {
    path:'login',
    loadChildren : () => import('./auth/auth.module').then(m=>m.AuthModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
