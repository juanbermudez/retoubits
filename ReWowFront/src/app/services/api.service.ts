import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SocialUser } from 'angularx-social-login';
import { Appointment } from '../models/appointment';
import { Pets } from '../models/pets';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  apiUrl='http://localhost:8080/api/'

  constructor(private http: HttpClient) { }

  getAll(){
    return this.http.get(this.apiUrl + 'allUsers', { headers:this.headers, observe: 'response' })
  }

  user(user:User){
    return this.http.post(this.apiUrl + 'user',user,{ headers:this.headers, observe: 'response' })
  }

  addPet(pet:Pets,id:number){
    return this.http.post(this.apiUrl + 'addPet/'+id,pet,{ headers:this.headers, observe: 'response' })
  }

  addAppointment(appointment:Appointment,id:number){
    return this.http.post(this.apiUrl + 'addAppointment/'+id,appointment,{ headers:this.headers, observe: 'response' })
  }
}
