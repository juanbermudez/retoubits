import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Pets } from '../models/pets';
import { User } from '../models/user';
import { ApiService } from '../services/api.service';
import { DialogAppointmentComponent } from './dialog-appointment/dialog-appointment.component';

@Component({
  selector: 'app-menu-appointment',
  templateUrl: './menu-appointment.component.html',
  styleUrls: ['./menu-appointment.component.scss']
})
export class MenuAppointmentComponent implements OnInit {

  userState:User;
  pets:Pets[]

  formAppointment:FormGroup=new FormGroup({
    dateTime: new FormControl('',Validators.required),
    typeAppointment:new FormControl('',Validators.required),
    petId:new FormControl('',Validators.required)
  })

  constructor(private api:ApiService,public dialog: MatDialog) { }

  ngOnInit(): void {
    this.userState = <User>history.state;
    this.pets=this.userState.pets;
  }

  addAppointment(formAppointment:FormGroup){
    this.api.addAppointment(formAppointment.value,formAppointment.value.petId).subscribe(data=>{
     this.openDialog(data.body);
    })
  }

  openDialog(data): void {
    const dialogRef = this.dialog.open(DialogAppointmentComponent, {
      width: '60%',
      data:data
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
  }

}
