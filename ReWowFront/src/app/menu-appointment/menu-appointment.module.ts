import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuAppointmentRoutingModule } from './menu-appointment-routing.module';
import { MenuAppointmentComponent } from './menu-appointment.component';
import { MaterialModules } from '../material-modules';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogAppointmentComponent } from './dialog-appointment/dialog-appointment.component';


@NgModule({
  declarations: [MenuAppointmentComponent, DialogAppointmentComponent],
  imports: [
    CommonModule,
    MenuAppointmentRoutingModule,
    MaterialModules,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class MenuAppointmentModule { }
