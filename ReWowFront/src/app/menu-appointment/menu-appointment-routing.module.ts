import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuAppointmentComponent } from './menu-appointment.component';


const routes: Routes = [
  {
    path:'',
    component:MenuAppointmentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuAppointmentRoutingModule { }
