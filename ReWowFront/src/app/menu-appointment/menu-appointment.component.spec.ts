import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuAppointmentComponent } from './menu-appointment.component';

describe('MenuAppointmentComponent', () => {
  let component: MenuAppointmentComponent;
  let fixture: ComponentFixture<MenuAppointmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuAppointmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuAppointmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
