import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  
  userState:User;

  constructor() { }

  ngOnInit(): void {
    this.userState = <User>history.state;
    console.log(this.userState)
  }

}
