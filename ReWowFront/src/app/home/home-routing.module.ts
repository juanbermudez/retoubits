import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';


const routes: Routes = [
  {
    path:'',
    component:HomeComponent,
    children:[
      {
        path:'misMascotas',
        loadChildren: ()=>import('../menu-my-pets/menu-my-pets.module').then(m=>m.MenuMyPetsModule),
      },
      {
        path:'historia',
        loadChildren: ()=>import('../menu-history/menu-history-routing.module').then(m=>m.MenuHistoryRoutingModule)
      },
      {
        path:'citas',
        loadChildren: ()=>import('../menu-appointment/menu-appointment.module').then(m=>m.MenuAppointmentModule)
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
