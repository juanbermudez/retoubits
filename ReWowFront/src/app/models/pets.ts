import { Appointment } from "./appointment";

export interface Pets {

    id:number;
    name:string;
    age:number;
    race:string;
    petType:string;
    petSize:string;
    description:string;
    appointments:Appointment[]
}
