import { Pets } from "./pets";

export interface User{
    id:number;
    name:string;
    email:string;
    provider:string;
    pets:Pets[];
}