export interface Appointment {
    id:number;
    dateTime:Date;
    typeAppointment:string;
}
