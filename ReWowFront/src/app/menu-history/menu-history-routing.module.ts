import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuHistoryComponent } from './menu-history.component';
import { MenuHistoryModule } from './menu-history.module';


const routes: Routes = [
  {
    path:'',
    component:MenuHistoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuHistoryRoutingModule { }
