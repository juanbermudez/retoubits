import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuHistoryRoutingModule } from './menu-history-routing.module';
import { MenuHistoryComponent } from './menu-history.component';
import { MaterialModules } from '../material-modules';



@NgModule({
  declarations: [MenuHistoryComponent],
  imports: [
    CommonModule,
    MenuHistoryRoutingModule,
    MaterialModules
  ]
})
export class MenuHistoryModule { }
