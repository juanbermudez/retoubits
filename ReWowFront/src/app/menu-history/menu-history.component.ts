import { Component, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { Pets } from '../models/pets';
import { User } from '../models/user';
import { ApiService } from '../services/api.service';


@Component({
  selector: 'app-menu-history',
  templateUrl: './menu-history.component.html',
  styleUrls: ['./menu-history.component.scss']
})
export class MenuHistoryComponent implements OnInit {

  userState:User;
  pets:Pets[]
  selected :Pets;
  constructor(private api:ApiService) { }

  ngOnInit(): void {
    this.userState = <User>history.state;
    this.api.user(this.userState).subscribe(data=>{
      this.userState=<User>data.body;
      this.pets=this.userState.pets;
    })
  }

}
