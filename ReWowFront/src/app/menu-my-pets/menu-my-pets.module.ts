import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuMyPetsRoutingModule } from './menu-my-pets-routing.module';
import { MenuMyPetsComponent } from './menu-my-pets.component';
import { MaterialModules } from '../material-modules';


@NgModule({
  declarations: [MenuMyPetsComponent],
  imports: [
    CommonModule,
    MenuMyPetsRoutingModule,
    MaterialModules
  ]
})
export class MenuMyPetsModule { }
