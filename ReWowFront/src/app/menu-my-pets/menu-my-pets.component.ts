import { Component, OnInit } from '@angular/core';
import { Pets } from '../models/pets';
import { User } from '../models/user';

export interface Section {
  name: string;
  updated: Date;
}

@Component({
  selector: 'app-menu-my-pets',
  templateUrl: './menu-my-pets.component.html',
  styleUrls: ['./menu-my-pets.component.scss']
})
export class MenuMyPetsComponent implements OnInit {

  userState:User;
  pets:Pets[]

  constructor() { }

  ngOnInit(): void {
    this.userState = <User>history.state;
    this.pets=this.userState.pets;
  }

}
