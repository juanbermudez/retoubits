import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuMyPetsComponent } from './menu-my-pets.component';


const routes: Routes = [
  {
    path:'',
    component:MenuMyPetsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuMyPetsRoutingModule { }
