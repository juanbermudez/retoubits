import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuMyPetsComponent } from './menu-my-pets.component';

describe('MenuMyPetsComponent', () => {
  let component: MenuMyPetsComponent;
  let fixture: ComponentFixture<MenuMyPetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuMyPetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuMyPetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
