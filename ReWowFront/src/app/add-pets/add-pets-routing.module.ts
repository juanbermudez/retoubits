import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPetsComponent } from './add-pets.component';


const routes: Routes = [
  {
    path:'',
    component:AddPetsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddPetsRoutingModule { }
