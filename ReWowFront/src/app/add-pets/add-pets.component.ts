import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Pets } from '../models/pets';
import { User } from '../models/user';
import { ApiService } from '../services/api.service';



@Component({
  selector: 'app-add-pets',
  templateUrl: './add-pets.component.html',
  styleUrls: ['./add-pets.component.scss']
})
export class AddPetsComponent implements OnInit {
 
  userState:User;
  formAddPet:FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    age: new FormControl('', [Validators.required]),
    race: new FormControl('', [Validators.required]),
    petType: new FormControl('', [Validators.required]),
    petSize: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
  })

  constructor(private api:ApiService,
    private router:Router) { }

  ngOnInit(): void {
    this.userState = <User>history.state;
  }

  addPet(formAddPet:FormGroup){
    this.api.addPet(formAddPet.value,this.userState.id).subscribe((data:HttpResponse<Pets>)=>{
      console.log(this.userState.pets==null)

      this.userState.pets==null
        ?this.userState.pets=[data.body] 
        :this.userState.pets.push(<Pets>data.body);
      
        console.log(this.userState.pets)

      this.router.navigate(['../../home/misMascotas'],{state:this.userState})
    })
  }

}
