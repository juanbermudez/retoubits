import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddPetsRoutingModule } from './add-pets-routing.module';
import { AddPetsComponent } from './add-pets.component';
import { MaterialModules } from '../material-modules';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [AddPetsComponent],
  imports: [
    CommonModule,
    AddPetsRoutingModule,
    MaterialModules,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class AddPetsModule { }
