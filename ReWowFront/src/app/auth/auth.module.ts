import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './auth.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';

import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider
} from 'angularx-social-login';
import { MaterialModules } from '../material-modules';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [AuthComponent],
  imports: [
    SocialLoginModule,
    CommonModule,
    AuthRoutingModule,
    MaterialModules,
    HttpClientModule,
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '599633359950-qdvsheid2bikablgkbl13ceslgnlujpk.apps.googleusercontent.com'
            )
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('777899839739496')
          }
        ]
      } as SocialAuthServiceConfig,
    }
  ]
})
export class AuthModule {
  
 }
