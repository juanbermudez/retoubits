import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService, SocialUser } from 'angularx-social-login';
import { User } from '../models/user';
import { ApiService } from '../services/api.service';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  loggedIn: boolean;
  
  constructor(private authService: SocialAuthService,
    private router:Router,
    private api:ApiService) { }
  
  ngOnInit() {
    this.authService.authState.subscribe((user) => {
      console.log(user)
      if ((user != null)) {
        sessionStorage.setItem('user',JSON.stringify(user));
        
        this.api.user({
          id:null,
          name:user.name,
          email:user.email,
          provider:user.provider,
          pets:null
        }).subscribe(data=>{
          console.log(data)
          this.router.navigate(['home'],{state:data.body});
        })
         
      }
    });
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.authService.signOut();
  }

  

  
}
