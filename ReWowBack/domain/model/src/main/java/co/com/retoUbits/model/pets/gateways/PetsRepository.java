package co.com.retoUbits.model.pets.gateways;

import co.com.retoUbits.model.pets.Pets;
import co.com.retoUbits.model.user.User;

public interface PetsRepository {

    Pets addPets(User user,Pets pets);
    boolean exitsById(Long id);
    void deleteById(Long id);
    Pets editPets(Pets pet);
    Pets findById(Long id);
}
