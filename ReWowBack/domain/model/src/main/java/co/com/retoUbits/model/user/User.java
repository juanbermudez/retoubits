package co.com.retoUbits.model.user;

import co.com.retoUbits.model.pets.Pets;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User{

    private Long id;
    private String name;
    private String email;
    private String provider;

    private List<Pets> pets;

}
