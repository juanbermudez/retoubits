package co.com.retoUbits.model.pets;

import co.com.retoUbits.model.appointment.Appointment;
import co.com.retoUbits.model.constant.PetSize;
import co.com.retoUbits.model.constant.PetType;
import co.com.retoUbits.model.vaccine.Vaccine;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Pets{

    private Long id;
    private String name;
    private Integer age;
    private String race;
    private PetType petType;
    private PetSize petSize;
    private String description;

    private List<Appointment> appointments;
    private List<Vaccine> vaccines;
}
