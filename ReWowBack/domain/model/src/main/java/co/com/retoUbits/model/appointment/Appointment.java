package co.com.retoUbits.model.appointment;

import co.com.retoUbits.model.constant.PetType;
import co.com.retoUbits.model.constant.TypeAppointment;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Appointment{

    private Long id;
    private LocalDateTime dateTime;
    private TypeAppointment typeAppointment;

}
