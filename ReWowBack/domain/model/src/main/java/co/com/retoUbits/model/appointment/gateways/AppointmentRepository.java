package co.com.retoUbits.model.appointment.gateways;

import co.com.retoUbits.model.appointment.Appointment;
import co.com.retoUbits.model.pets.Pets;

public interface AppointmentRepository {

    Appointment addAppointment(Pets pets,Appointment appointment);
}
