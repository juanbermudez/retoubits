package co.com.retoUbits.model.constant;

import co.com.retoUbits.model.appointment.Appointment;

public enum TypeAppointment {

    MEDICAL_APPOINTMENT,
    VACCINATION_APPOINTMENT,
    DEWORMING_APPOINTMENT,
    BATH_APPOINTMENT,
    HAIRCUT_APPOINTMENT

}
