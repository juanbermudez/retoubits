package co.com.retoUbits.model.vaccine;

import lombok.*;

import java.time.LocalDate;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Vaccine{

    private Long id;
    private String name;
    private String dose;
    private LocalDate date;
}
