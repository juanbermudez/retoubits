package co.com.retoUbits.model.user.gateways;

import co.com.retoUbits.model.user.User;

import java.util.List;

public interface UserRepository {

    User addUser(User user);
    boolean existsByEmail(String email);
    User findByEmail(String email);
    User findById(Long id);
    User editUser(User user);
    boolean exitsById(Long id);
    void deleteById(Long id);
    List<User> getAll();
}
