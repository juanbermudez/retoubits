package co.com.retoUbits.usecase.appointment;

import co.com.retoUbits.model.appointment.Appointment;
import co.com.retoUbits.model.appointment.gateways.AppointmentRepository;
import co.com.retoUbits.model.pets.Pets;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AppointmentUseCase {

    private final AppointmentRepository appointmentRepository;

    public Appointment addPets(Pets pets, Appointment appointment){
        return appointmentRepository.addAppointment(pets,appointment);
    }

}
