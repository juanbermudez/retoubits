package co.com.retoUbits.usecase.user;

import co.com.retoUbits.model.user.User;
import co.com.retoUbits.model.user.gateways.UserRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class UserUseCase {

    private final UserRepository userRepository;

    public User addUser(User user){
        return userRepository.addUser(user);
    }

    public boolean exitsByEmail(String email){
        return userRepository.existsByEmail(email);
    }

    public User findByEmail(String email){
        return userRepository.findByEmail(email);
    }

    public User findById(Long id) {
        return userRepository.findById(id);
    }

    public User editUser(User user) {
        return userRepository.editUser(user);
    }


    public boolean exitsById(Long id) {
        return userRepository.exitsById(id);
    }

    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    public List<User> getAll() {
        return userRepository.getAll();
    }
}
