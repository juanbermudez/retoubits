package co.com.retoUbits.usecase.pets;

import co.com.retoUbits.model.pets.Pets;
import co.com.retoUbits.model.pets.gateways.PetsRepository;
import co.com.retoUbits.model.user.User;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PetsUseCase {

    private final PetsRepository petsRepository;

    public Pets addPets(User user, Pets pets){
        return petsRepository.addPets(user,pets);
    }

    public boolean exitsById(Long id) {
        return petsRepository.exitsById(id);
    }

    public void deleteById(Long id) {
        petsRepository.deleteById(id);
    }

    public Pets editPets(Pets pet) {
        return petsRepository.editPets(pet);
    }

    public Pets findById(Long id) {
        return petsRepository.findById(id);
    }
}
