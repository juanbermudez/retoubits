package co.com.retoUbits.api;

import co.com.retoUbits.model.appointment.Appointment;
import co.com.retoUbits.model.pets.Pets;
import co.com.retoUbits.model.user.User;
import co.com.retoUbits.usecase.appointment.AppointmentUseCase;
import co.com.retoUbits.usecase.pets.PetsUseCase;
import co.com.retoUbits.usecase.user.UserUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.nio.file.AccessDeniedException;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class ApiRest {

    private final UserUseCase userUseCase;
    private final PetsUseCase petsUseCase;
    private final AppointmentUseCase appointmentUseCase;

    /*----------------OPERACIONES DE USUARIOS----------------*/

    @GetMapping(path = "allUsers")
    public ResponseEntity<List<User>> allUsers(){
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userUseCase.getAll());
    }

    @PostMapping(path = "user")
    public ResponseEntity<User> user(@RequestBody User user){
        /*
        ESTE METODO REGISTRA LOS USUARIOS Y EN CASO DE YA ESTAR REGISTRADO EN BD
        RETORNA EL EXISTENTE
         */
        boolean exitsByEmail = userUseCase.exitsByEmail(user.getEmail());
        return ResponseEntity
                .status(exitsByEmail?HttpStatus.OK:HttpStatus.CREATED)
                .body(exitsByEmail?userUseCase.findByEmail(user.getEmail()):userUseCase.addUser(user));
    }

    @PutMapping(path = "user/{user_id}")
    public ResponseEntity<User> editUser(@RequestBody User user,@PathVariable("user_id") Long id){
        /*
        ESTE METODO EDITA LOS USUARIOS EXISTENTES EN EL MODELO DEL USUARIO
         */
        boolean exitsById = userUseCase.exitsById(id);
        return ResponseEntity
                .status(exitsById?HttpStatus.OK:HttpStatus.NOT_FOUND)
                .body(exitsById?userUseCase.editUser(user):null);
    }

    @DeleteMapping(path = "user/{user_id}")
    public ResponseEntity deleteUser(@PathVariable("user_id") Long id){
        /*
        ESTE METODO ELIMINA LA
         */
        if (userUseCase.exitsById(id)){
            userUseCase.deleteById(id);
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }
    /*--------------------------------------------------*/

    /*----------------OPERACIONES DE MASCOTAS----------------*/

    @PostMapping(path = "addPet/{user_id}")
    public ResponseEntity<Pets> addPet(@RequestBody Pets pets,@PathVariable("user_id") Long id){
        /*
        ESTE METODO REGISTRA MASCOTAS A USUARIOS EXISTENTES EN BD
         */
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(petsUseCase.addPets(userUseCase.findById(id),pets));
    }

    @PutMapping(path = "pet/{pet_id}")
    public ResponseEntity<Pets> editUser(@RequestBody Pets pet,@PathVariable("pet_id") Long id){
        /*
        ESTE METODO EDITA LOS USUARIOS EXISTENTES EN EL MODELO DEL USUARIO
         */
        boolean exitsById = petsUseCase.exitsById(id);
        return ResponseEntity
                .status(exitsById?HttpStatus.OK:HttpStatus.NOT_FOUND)
                .body(exitsById?petsUseCase.editPets(pet):null);
    }

    @DeleteMapping(path = "pet/{pet_id}")
    public ResponseEntity deletePet(@PathVariable("pet_id") Long id){
        /*
        ESTE METODO ELIMINA LA
         */
        if (petsUseCase.exitsById(id)){
            petsUseCase.deleteById(id);
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    /*--------------------------------------------------*/

    /*----------------OPERACIONES DE CITAS----------------*/

    @PostMapping(path = "addAppointment/{pet_id}")
    public ResponseEntity<Appointment> addPet(@RequestBody Appointment appointment, @PathVariable("pet_id") Long id){
        /*
        ESTE METODO REGISTRA MASCOTAS A USUARIOS EXISTENTES EN BD
         */
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(appointmentUseCase.addPets(petsUseCase.findById(id),appointment));
    }

    /*--------------------------------------------------*/

}

