package co.com.retoUbits.jpa.userData;

import co.com.retoUbits.model.user.User;
import co.com.retoUbits.model.user.gateways.UserRepository;
import lombok.RequiredArgsConstructor;
import org.reactivecommons.utils.ObjectMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserDataAdapter implements UserRepository {

    private final UserDataRepository userDataRepository;
    private final ObjectMapper mapper;

    @Override
    public User addUser(User user) {
        UserData userData = mapper.map(user,UserData.class);
        return mapper.map(userDataRepository.save(userData),User.class);
    }

    @Override
    public boolean existsByEmail(String email) {
        return userDataRepository.existsByEmail(email);
    }

    @Override
    public User findByEmail(String email) {
        return mapper.map(userDataRepository.findByEmail(email),User.class);
    }

    @Override
    public User findById(Long id) {
        return mapper.map(userDataRepository.findById(id).get(),User.class);
    }

    @Override
    public User editUser(User user) {
        UserData userEdit = userDataRepository.findById(user.getId()).get();
        userEdit.setName(user.getName());
        userEdit.setProvider(user.getProvider());
        return mapper.map(userDataRepository.save(userEdit),User.class);
    }

    @Override
    public boolean exitsById(Long id) {
        return userDataRepository.existsById(id);
    }

    @Override
    public void deleteById(Long id) {
        userDataRepository.deleteById(id);
    }

    @Override
    public List<User> getAll() {
        List<UserData> usersData = userDataRepository.findAll();
        return usersData.stream()
                .map(userData -> mapper.map(userData,User.class))
                .collect(Collectors.toList());
    }
}
