package co.com.retoUbits.jpa.petsData;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PetsDataRepository extends JpaRepository<PetsData,Long> {

}
