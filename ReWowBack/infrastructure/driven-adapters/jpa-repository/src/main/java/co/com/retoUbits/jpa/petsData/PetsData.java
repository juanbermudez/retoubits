package co.com.retoUbits.jpa.petsData;

import co.com.retoUbits.jpa.appointmentData.AppointmentData;
import co.com.retoUbits.jpa.constantData.PetSizeData;
import co.com.retoUbits.jpa.constantData.PetTypeData;
import co.com.retoUbits.jpa.userData.UserData;
import co.com.retoUbits.jpa.vaccineData.VaccineData;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "MASCOTAS")
public class PetsData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NOMBRE")
    private String name;

    @Column(name = "EDAD")
    private Integer age;

    @Column(name = "RAZA")
    private String race;

    @Column(name = "TIPO")
    @Enumerated(EnumType.ORDINAL)
    private PetTypeData petType;

    @Column(name = "TAMANO")
    @Enumerated(EnumType.ORDINAL)
    private PetSizeData petSize;

    @Column(name = "DESCRIPCION")
    private String description;

    /*
    RELACCIONES
     */
    @ManyToOne
    @JoinColumn(name="USUARIO_ID")
    private UserData user;

    @Column(name = "MASCOTAS")
    @OneToMany(mappedBy = "pet", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
    private List<AppointmentData> appointments;

    @Column(name = "VACUNAS")
    @OneToMany(mappedBy = "pet", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
    private List<VaccineData> vaccines;
}
