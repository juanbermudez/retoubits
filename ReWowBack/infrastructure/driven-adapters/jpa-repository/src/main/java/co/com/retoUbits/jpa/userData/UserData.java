package co.com.retoUbits.jpa.userData;

import co.com.retoUbits.jpa.petsData.PetsData;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "USUARIOS")
public class UserData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NOMBRE")
    private String name;

    @Column(name = "CORREO_ELECTRONICO")
    private String email;

    @Column(name = "PROVEEDOR_OAUT")
    private String provider;

    /*
    RELACIONES
     */
    @Column(name = "MASCOTAS")
    @OneToMany(mappedBy = "user", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
    List<PetsData> pets;

}
