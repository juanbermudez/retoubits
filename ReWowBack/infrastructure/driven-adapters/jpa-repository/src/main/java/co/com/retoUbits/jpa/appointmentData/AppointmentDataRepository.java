package co.com.retoUbits.jpa.appointmentData;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AppointmentDataRepository extends JpaRepository<AppointmentData,Long> {
}
