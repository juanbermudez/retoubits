package co.com.retoUbits.jpa.constantData;

public enum TypeAppointmentData {

    MEDICAL_APPOINTMENT,
    VACCINATION_APPOINTMENT,
    DEWORMING_APPOINTMENT,
    BATH_APPOINTMENT,
    HAIRCUT_APPOINTMENT

}
