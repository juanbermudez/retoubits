package co.com.retoUbits.jpa.userData;

import co.com.retoUbits.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDataRepository extends JpaRepository<UserData,Long> {

    boolean existsByEmail(String email);
    UserData findByEmail(String email);
}
