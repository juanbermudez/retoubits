package co.com.retoUbits.jpa.petsData;

import co.com.retoUbits.jpa.userData.UserData;
import co.com.retoUbits.model.pets.Pets;
import co.com.retoUbits.model.pets.gateways.PetsRepository;
import co.com.retoUbits.model.user.User;
import lombok.RequiredArgsConstructor;
import org.reactivecommons.utils.ObjectMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class PetsDataAdapter implements PetsRepository {

    private final PetsDataRepository petsDataRepository;
    private final ObjectMapper mapper;

    @Override
    public Pets addPets(User user, Pets pets) {
        UserData userData = mapper.map(user,UserData.class);
        PetsData petsData = mapper.map(pets,PetsData.class);
        petsData.setUser(userData);
        return mapper.map(petsDataRepository.save(petsData),Pets.class);
    }

    @Override
    public boolean exitsById(Long id) {
        return petsDataRepository.existsById(id);
    }

    @Override
    public void deleteById(Long id) {
        petsDataRepository.deleteById(id);
    }

    @Override
    public Pets editPets(Pets pet) {
        PetsData petsData = mapper.map(pet,PetsData.class);
        PetsData petsEdit = petsDataRepository.findById(pet.getId()).get();
        petsEdit.setName(petsData.getName());
        petsEdit.setPetSize(petsData.getPetSize());
        petsEdit.setPetSize(petsData.getPetSize());
        petsEdit.setDescription(petsData.getDescription());
        return mapper.map(petsDataRepository.save(petsEdit),Pets.class);
    }

    @Override
    public Pets findById(Long id) {
        return mapper.map(petsDataRepository.findById(id).get(),Pets.class);
    }
}
