package co.com.retoUbits.jpa.appointmentData;

import co.com.retoUbits.jpa.petsData.PetsData;
import co.com.retoUbits.model.appointment.Appointment;
import co.com.retoUbits.model.appointment.gateways.AppointmentRepository;
import co.com.retoUbits.model.pets.Pets;
import lombok.RequiredArgsConstructor;
import org.reactivecommons.utils.ObjectMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AppointmentDataAdapter implements AppointmentRepository {

    private final AppointmentDataRepository appointmentDataRepository;
    private final ObjectMapper mapper;

    @Override
    public Appointment addAppointment(Pets pets, Appointment appointment) {
        PetsData petsData = mapper.map(pets,PetsData.class);
        AppointmentData appointmentData = mapper.map(appointment,AppointmentData.class);
        appointmentData.setPet(petsData);
        return mapper.map(appointmentDataRepository.save(appointmentData),Appointment.class);
    }
}
