package co.com.retoUbits.jpa.vaccineData;

import co.com.retoUbits.jpa.petsData.PetsData;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "VACUNA")
public class VaccineData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NOMBRE")
    private String name;

    @Column(name = "DOSIS")
    private String dose;

    @Column(name = "FECHA")
    private LocalDate date;

    /*
    RELACIONES
     */
    @ManyToOne
    @JoinColumn(name="MASCOTA_ID")
    private PetsData pet;
}
