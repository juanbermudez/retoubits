package co.com.retoUbits.jpa.appointmentData;

import co.com.retoUbits.jpa.constantData.TypeAppointmentData;
import co.com.retoUbits.jpa.petsData.PetsData;
import co.com.retoUbits.jpa.userData.UserData;
import co.com.retoUbits.model.constant.PetType;
import co.com.retoUbits.model.constant.TypeAppointment;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "CITAS")
public class AppointmentData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "FECHA")
    private LocalDateTime dateTime;

    @Column(name = "TIPO_CITA")
    private TypeAppointmentData typeAppointment;

    /*
    RELACIONES
     */
    @ManyToOne
    @JoinColumn(name="MASCOTA_ID")
    private PetsData pet;
}
