package co.com.retoUbits.config;

import co.com.retoUbits.model.appointment.gateways.AppointmentRepository;
import co.com.retoUbits.model.pets.gateways.PetsRepository;
import co.com.retoUbits.model.user.gateways.UserRepository;
import co.com.retoUbits.usecase.appointment.AppointmentUseCase;
import co.com.retoUbits.usecase.pets.PetsUseCase;
import co.com.retoUbits.usecase.user.UserUseCase;
import org.reactivecommons.utils.ObjectMapper;
import org.reactivecommons.utils.ObjectMapperImp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCaseConfig {

    @Bean
    public UserUseCase userCaseConfig(UserRepository userRepository){
        return new UserUseCase(userRepository);
    }

    @Bean
    public PetsUseCase petsUseCaseConfig(PetsRepository petsRepository){
        return new PetsUseCase(petsRepository);
    }

    @Bean
    public AppointmentUseCase appointmentCaseConfig(AppointmentRepository appointmentRepository){
        return new AppointmentUseCase(appointmentRepository);
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapperImp();
    }
}
